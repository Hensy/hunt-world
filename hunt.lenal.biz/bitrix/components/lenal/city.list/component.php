<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 180;

/*************************************************************************
	Получение параметров
*************************************************************************/
$arParams['IBLOCK_TYPE'] = $arParams['IBLOCK_TYPE'];
$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);?>


<?if($arParams['IBLOCK_ID'] > 0 && $this->StartResultCache(false, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups())))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	//SELECT
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"IBLOCK_SECTION_ID",
		"NAME",
                "PROPERTY_CITY_PHONE"
            
	);
	//WHERE
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE_DATE" => "Y",
		"ACTIVE"=>"Y",
		"CHECK_PERMISSIONS"=>"Y",
            
            
		
	);
	//ORDER BY
	$arSort = array();
	//EXECUTE
        
	$rsIBlockElement = CIBlockElement::GetList($arSort, $arFilter, false, false,$arSelect);
        $rsIBlockElement->SetUrlTemplates($arParams["DETAIL_URL"]);
        $arResult = array();
	while($arRes = $rsIBlockElement->GetNext())
	{
          $arResult[] =  $arRes;
        }
         
}?>

        
       <? if($arResult)
        {
            
            $this->SetResultCacheKeys(array(
                "ID",
                "IBLOCK_TYPE_ID",
                "NAME"
		));
	    $this->IncludeComponentTemplate();
        }
	else
	{
		$this->AbortResultCache();
	}

?>
            
