$(document).ready(function()
{
    $(".city").click(function()
   {
       $(".city_list").toggle("fast");
   }) ;
});
   

function saveLocation(city_name,template_path)
{
    //save for Javascript
    localStorage.setItem("city", city_name);
    
    
    //set SESSION 
    $.ajax({
        type: "POST",
        url: template_path+"/ajax.php",
        dataType: "html",
        data: {city_name: city_name},
        success: function(html)
        {
            $(".city").text(html);
        }
             });
           
}