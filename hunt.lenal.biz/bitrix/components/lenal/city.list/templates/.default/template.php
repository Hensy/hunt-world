<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<span class="city">

    <?
    if (CModule::IncludeModule("altasib.geoip")) {
        $arData = ALX_GeoIP::GetAddr();
    }
    if(empty($_SESSION["city_name"]))
    { print_r($arData["city"]);}
    else 
    {
        echo ($_SESSION["city_name"]);
    }

    ?>
</span>
<ul class="city_list">
    <? foreach ($arResult as $arItem): ?>
        <li><a href="javascript:void(0)" onclick="saveLocation('<?= $arItem["NAME"] ?>', '<?= $templateFolder ?>')"><?= $arItem["NAME"] ?></a></li>
    <? endforeach; ?>
</ul>
<script>
    $(document).ready(function()
{
    $(".city").click(function()
   {
       $(".city_list").toggle("fast");
   }) ;
});
   

function saveLocation(city_name,template_path)
{
    //save for Javascript
    localStorage.setItem("city", city_name);
    
    
    //set SESSION 
    $.ajax({
        type: "POST",
        url: template_path+"/ajax.php",
        data: {city_name: city_name},
        success: function(html)
        {
            $(".city").text(html);
        }
             });
           
}
</script>    



