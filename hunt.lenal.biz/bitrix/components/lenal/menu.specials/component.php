<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 180;


$arParams['IBLOCK_TYPE'] = $arParams['IBLOCK_TYPE'];
$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
?>


<?
if ($arParams['IBLOCK_ID'] > 0 && $this->StartResultCache(false, ($arParams["CACHE_GROUPS"] === "N" ? false : $USER->GetGroups()))) {
    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $arFilter = Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', "DEPTH_LEVEL" => 1);
    $rsSections = CIBlockSection::GetList(Array($by => $order), $arFilter, false, array("ID", "NAME", "CODE"), false);

    $arResult = array();
    $i = 0;
    while ($arSection = $rsSections->Fetch()) {
        $arResult["SECTIONS"][$i] = array(
            "ID" => $arSection["ID"],
            "NAME" => $arSection["NAME"],
            "CODE" => $arSection["CODE"]
        );
        $arSubFilter = Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], "SECTION_ID" => $arSection["ID"]);
        $rsSubSections = CIBlockSection::GetList(Array($by => $order), $arSubFilter, false, array("ID", "NAME", "CODE","UF_BANNER_PIC"), false);

        $j = 0;
        while ($arSubSection = $rsSubSections->Fetch()) {
            $arResult["SECTIONS"][$i]["SUBSECTIONS"][$j] = array(
                "ID" => $arSubSection["ID"],
                "NAME" => $arSubSection["NAME"],
                "CODE" => $arSubSection["CODE"],
                "BANNER_PIC"=>$arSubSection["UF_BANNER_PIC"]
            );


            $rsParentSection = CIBlockSection::GetByID($arSubSection["ID"]);
            if ($arParentSection = $rsParentSection->GetNext()) {
                $subsections_array = array(0 => $arSubSection["ID"]);
                $arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'], '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // ������� �������� ��� ����� ����������
                $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);
                while ($arSect = $rsSect->GetNext()) {
                    $subsections_array[] = $arSect["ID"];
                }
            }
            //print_r($subsections_array);

            
            //Find selected specials
            $arSort = array("SORT" => "ASC");
            $arFilter = array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_SHOW_SPEC_IN_MENU_VALUE" => "��", "SECTION_ID" => $subsections_array);
            $arSelect = array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID", "IBLOCK_ID", "PROPERTY_SHOW_SPEC_IN_MENU");
            $rsElement = CIBlockElement::GetList(
                            $arSort, $arFilter, false, false, $arSelect
            );
            while ($ob = $rsElement->Fetch()) {
                $arResult["SECTIONS"][$i]["SUBSECTIONS"][$j]["SELECTED_SPECIALS"][] = array(
                    "ID" => $ob["ID"],
                    "NAME" => $ob["NAME"],
                    "CODE" => $ob["CODE"],
                    "DETAIL_PICTURE"=>$ob["DETAIL_PICTURE"]
                );
            }
           $j++;
        }
        $i++;
    }
}
?>

    <?
    if ($arResult) {

        $this->SetResultCacheKeys(array(
            "ID",
            "IBLOCK_TYPE_ID",
            "NAME"
        ));
        $this->IncludeComponentTemplate();
    } else {
        $this->AbortResultCache();
    }
    ?>
            












