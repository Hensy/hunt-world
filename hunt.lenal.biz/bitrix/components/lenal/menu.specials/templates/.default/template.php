<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="menu_line">
    <div class="inner_wrapper_min">
        <ul class="menu">


            <? foreach ($arResult["SECTIONS"] as $arSection): ?>    
                <li ><a href="/catalog/<?= $arSection["CODE"] ?>/"><? echo($arSection["NAME"]) ?></a>
                    <? if (!empty($arSection["SUBSECTIONS"])): ?>
                        <div class="menu_second">
                            <ul>
                                <? foreach ($arSection["SUBSECTIONS"] as $arSubSection): ?>
                                    <li id="section<?= $arSubSection["ID"] ?>"><a href="/catalog/<?= $arSection["CODE"] ?>/<?= $arSubSection["CODE"] ?>/"><?= $arSubSection["NAME"] ?></a></li>
                                <? endforeach ?>
                                <li><a href="#">������� ��������������</a></li>
                                <li><a href="#">��� ��������� �����</a></li>
                            </ul>



                            <!--Specials-->
                            <? foreach ($arSection["SUBSECTIONS"] as $arSubSection): ?>
                                <? if (!empty($arSubSection["SELECTED_SPECIALS"])): ?>
                                    <div class="menu_offers left section<?= $arSubSection["ID"] ?>">
                                        <p class="menu_offers_title"><a href="#">����������� �����������</a></p>

                                        <div class="menu_offers_block">
                                            <? foreach ($arSubSection["SELECTED_SPECIALS"] as $special_item): ?>
                                                <div class="item">
                                                    <div class="item_top">
                                                        <a href="#">
                                                            <? $resized_image = CFile::ResizeImageGet($special_item["DETAIL_PICTURE"], array('width' => 118, 'height' => 118), BX_RESIZE_IMAGE_EXACT, true); ?>
                                                            <img src="<?= $resized_image["src"] ?>"> 
                                                        </a>
                                                        <!--Rating-->
                                                        <div class="item_top_rating">
                                                            <?
                                                            $element_rate_value = "";
                                                            foreach ($GLOBALS["rating_array"] as $key => $rate_value) {
                                                                if ($special_item["ID"] == $key) {
                                                                    $element_rate_value = $rate_value;


                                                                    //Get elements count
                                                                    if (!empty($rate_value)) {
                                                                        $rating_votes = count($element_rate_value);
                                                                    } else {
                                                                        $rating_votes = 0;
                                                                    }

                                                                    //Get elements sum
                                                                    $rating_sum = 0;
                                                                    foreach ($element_rate_value as $rate_element) {
                                                                        $rating_sum+=$rate_element;
                                                                    }
                                                                    //echo($rating_sum);
                                                                    $sorted_rating_stars[$special_item["ID"]] = array_count_values($element_rate_value);
                                                                }
                                                            }
                                                            if (!empty($element_rate_value)) {
                                                                //Average mark    
                                                                $rate_mark = round($rating_sum / $rating_votes);
                                                            } else {
                                                                //If no marks yet
                                                                $rate_mark = 0;
                                                                $rating_votes = 0;
                                                            }
                                                            ?>
                                                            <?
                                                            $star1count = ($sorted_rating_stars[$special_item["ID"]][1] / $rating_votes) * 100;
                                                            $star2count = ($sorted_rating_stars[$special_item["ID"]][2] / $rating_votes) * 100;
                                                            $star3count = ($sorted_rating_stars[$special_item["ID"]][3] / $rating_votes) * 100;
                                                            $star4count = ($sorted_rating_stars[$special_item["ID"]][4] / $rating_votes) * 100;
                                                            $star5count = ($sorted_rating_stars[$special_item["ID"]][5] / $rating_votes) * 100;
                                                            ?>
                                                            <span class="stars small star<?= $rate_mark ?>"></span>



                                                            <span class="estimate"><?= $rating_votes ?> <? echo pluralForm($rating_votes, '������', '������', '������') ?></span>
                                                            <div class="show_rating">
                                                                <div class="rating_line"><div style="width:<?= $star1count ?>%;"> <?= $sorted_rating_stars[$special_item["ID"]][1] ?> </div></div>
                                                                <div class="rating_line"><div style="width:<?= $star2count ?>%;"> <?= $sorted_rating_stars[$special_item["ID"]][2] ?> </div></div>
                                                                <div class="rating_line"><div style="width:<?= $star3count ?>%;"> <?= $sorted_rating_stars[$special_item["ID"]][3] ?> </div></div>
                                                                <div class="rating_line"><div style="width:<?= $star4count ?>%;"> <?= $sorted_rating_stars[$special_item["ID"]][4] ?> </div></div>
                                                                <div class="rating_line"><div style="width:<?= $star5count ?>%;"> <?= $sorted_rating_stars[$special_item["ID"]][5] ?> </div></div>
                                                            </div>    
                                                        </div>


                                                    </div>
                                                    <div class="item_bottom">
                                                        <div class="price">

                                                            <!--���� � ������ ��� ��������-->
                                                            <?
                                                            $ar_res = GetCatalogProductPrice($special_item["ID"], 1);
                                                            $product_full_price = $ar_res["PRICE"];

                                                            //Check on discounts
                                                            $arDiscounts = CCatalogDiscount::GetDiscountByProduct($special_item["ID"], $USER->GetUserGroupArray(), "N", 2, SITE_ID);
                                                            $final_product_price = $product_full_price * ((100 - intval($arDiscounts[0]["VALUE"])) / 100);
                                                            ?>

                                                            <!--���� ����� �������� ��������-->
                                                            <?
                                                            if ($product_full_price == ""):


                                                                $intIBlockID = 2; //ID ��������� 
                                                                $mxResult = CCatalogSKU::GetInfoByProductIBlock(
                                                                                $intIBlockID
                                                                );
                                                                $array_min_prices = array();
                                                                if (is_array($mxResult)) {

                                                                    $rsOffers = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $mxResult['IBLOCK_ID'], 'PROPERTY_' . $mxResult['SKU_PROPERTY_ID'] => $special_item["ID"]));
                                                                    while ($arOffer = $rsOffers->GetNext()) {
                                                                        $ar_price = GetCatalogProductPrice($arOffer["ID"], 1); // 1 � ������ ������ ��� ����
                                                                        //Check on discounts 

                                                                        $arDiscounts = CCatalogDiscount::GetDiscountByProduct($arOffer["ID"], $USER->GetUserGroupArray(), "N", 2, SITE_ID);

                                                                        //Final price with discount
                                                                        $final_price = $ar_price['PRICE'] * ((100 - intval($arDiscounts[0]["VALUE"])) / 100);

                                                                        $array_min_prices[] = $ar_price;
                                                                        $array_min_prices_discount[] = $final_price;
                                                                    }
                                                                    $min_price_found = min($array_min_prices);
                                                                    $min_price_discount_found = min($array_min_prices_discount);
                                                                }
                                                                ?> 

                                                                <? if (round($min_price_found["PRICE"]) != round($min_price_discount_found)): ?>
                                                                    <span class="old_price"><? echo round($min_price_found["PRICE"]) ?> ���.</span> /
                                                                <? endif ?> 
                                                                <? if (round($min_price_discount_found) != '0' || round($min_price_found) != '0' && empty($min_price_discount_found)): ?>
                                                                    <span class="new_price"><?= round($min_price_discount_found) ?> ���</span>
                                                                <? endif ?>
                                                                    
                                                                    
                                                                <!--���� � ������ ��� ��������--->   

                                                            <? else: ?>   

                                                                <? if (round($product_full_price) != round($final_product_price)): ?>
                                                                    <span class="old_price"><? echo round($product_full_price) ?> ���.</span> /
                                                                <? endif ?> 
                                                                <? if (round($final_product_price) != '0' || round($product_full_price) != '0' && empty($final_product_price)): ?>
                                                                    <span class="new_price"><?= round($final_product_price) ?> ���</span>
                                                                <? endif ?>

                                                            <? endif ?>    
                                                        </div>
                                                        <div class="description">
                                                            <?= $special_item["NAME"] ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endforeach ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                <? endif; ?>
                            <? endforeach ?>

                            <!--Banners-->
                            <? foreach ($arSection["SUBSECTIONS"] as $arSubSection): ?>
                                <div class="menu_banners left section<?= $arSubSection["ID"] ?>">
                                    <? foreach ($arSubSection["BANNER_PIC"] as $banner_img): ?>
                                        <? $resized_image = CFile::ResizeImageGet($banner_img, array('width' => 265, 'height' => 120), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
                                        <div class="menu_banner_item"><a href="#"><img src="<?= $resized_image["src"] ?>"></a></div>
                                    <? endforeach; ?>
                                </div>
                                <div class="clear"></div>
                            <? endforeach ?> 
                        </div>
                    <? endif ?>
                </li>    

            <? endforeach; ?>    
        </ul>
    </div>
</div>