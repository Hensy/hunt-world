<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/" . SITE_TEMPLATE_ID . "/header.php");
$wizTemplateId = COption::GetOptionString("main", "wizard_template_id", "eshop_adapt_horizontal", SITE_ID);
CUtil::InitJSCore();
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
            <link rel="shortcut icon" type="image/x-icon" href="<?= SITE_DIR ?>/favicon.ico" />
            <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
            <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/js/reveal/reveal.css">
                <script src="<?= SITE_TEMPLATE_PATH ?>/js/reveal/jquery.reveal.js"></script>


                <?
                //$APPLICATION->ShowHead();
                echo '<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET . '"' . (true ? ' /' : '') . '>' . "\n";
                $APPLICATION->ShowMeta("robots", false, true);
                $APPLICATION->ShowMeta("keywords", false, true);
                $APPLICATION->ShowMeta("description", false, true);
                $APPLICATION->ShowCSS(true, true);
                ?>
                <link rel="stylesheet" type="text/css" href="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH . "/colors.css") ?>" />
                <?
                $APPLICATION->ShowHeadStrings();
                $APPLICATION->ShowHeadScripts();
                $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/script.js");
                ?>

                <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,100&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
                        <link href="<?= SITE_TEMPLATE_PATH ?>/css/helpers.css" rel="stylesheet" />
                        <link href="<?= SITE_TEMPLATE_PATH ?>/css/styles.css" rel="stylesheet" />

                        <script src="<?= SITE_TEMPLATE_PATH ?>/js/main.js"></script>
                        <script src="<?= SITE_TEMPLATE_PATH ?>/js/slider/jquery.bxslider.min.js"></script>
                        <link href="<?= SITE_TEMPLATE_PATH ?>/js/slider/jquery.bxslider.css" rel="stylesheet" />

                        <title><? $APPLICATION->ShowTitle() ?></title>
                        </head>
                        <body>
                            <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>

                            <div class="outer_wrapper header">
                                <div class="inner_wrapper_min">
                                    <div id="header">
                                        <div class="top_logo left w12">
                                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/logo.php"), false); ?>
                                        </div>
                                        <div class="header_inner right">
                                            <div class="header_top">
                                                <div class="w40 left">
                                                    <div class="dib top_menu">
                                                        <?
                                                        $APPLICATION->IncludeComponent('bitrix:menu', "top_menu", array(
                                                            "ROOT_MENU_TYPE" => "top",
                                                            "MENU_CACHE_TYPE" => "Y",
                                                            "MENU_CACHE_TIME" => "36000000",
                                                            "MENU_CACHE_USE_GROUPS" => "Y",
                                                            "MENU_CACHE_GET_VARS" => array(),
                                                            "MAX_LEVEL" => "1",
                                                            "USE_EXT" => "N",
                                                            "ALLOW_MULTI_SELECT" => "N"
                                                                )
                                                        );
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="magazine w20 left">
                                                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/magazine.php"), false); ?>

                                                </div>
                                                <div class="cabinet w15 left">
                                                    <?
                                                    $APPLICATION->IncludeComponent("bitrix:system.auth.form", "eshop_adapt", array(
                                                        "REGISTER_URL" => SITE_DIR . "login/",
                                                        "PROFILE_URL" => SITE_DIR . "personal/",
                                                        "SHOW_ERRORS" => "N"
                                                            ), false, array()
                                                    );
                                                    ?>
                                                </div>
                                                <div class="basket left w15">

                                                    <?
                                                    $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", ".default", array(
                                                        "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                                                        "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                                                        "SHOW_PERSONAL_LINK" => "N",
                                                        "SHOW_NUM_PRODUCTS" => "Y",
                                                        "SHOW_TOTAL_PRICE" => "Y",
                                                        "SHOW_PRODUCTS" => "Y",
                                                        "POSITION_FIXED" => "N",
                                                        "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                                                        "SHOW_DELAY" => "Y",
                                                        "SHOW_NOTAVAIL" => "Y",
                                                        "SHOW_SUBSCRIBE" => "Y",
                                                        "SHOW_IMAGE" => "Y",
                                                        "SHOW_PRICE" => "Y",
                                                        "SHOW_SUMMARY" => "Y"
                                                            ), false
                                                    );
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="header_bottom">
                                                <div class="top_contacts left w50">

                                                    <?
                                                    $APPLICATION->IncludeComponent("lenal:city.list", "city_list", array(
                                                        "IBLOCK_TYPE" => "city_list",
                                                        "IBLOCK_ID" => "4",
                                                        "CACHE_TYPE" => "A",
                                                        "CACHE_TIME" => "36000000",
                                                        "CACHE_GROUPS" => "Y"
                                                            ), false
                                                    );
                                                    ?>

                                                    
                                                    <a href="#" class="callback" data-reveal-id="myModal">�������� ������</a>

                                                </div>
                                                <div class="search left w50">

                                                    <?
                                                    $APPLICATION->IncludeComponent("bitrix:search.title", "catalog", array(
                                                        "NUM_CATEGORIES" => "1",
                                                        "TOP_COUNT" => "5",
                                                        "CHECK_DATES" => "N",
                                                        "SHOW_OTHERS" => "N",
                                                        "PAGE" => SITE_DIR . "catalog/",
                                                        "CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
                                                        "CATEGORY_0" => array(
                                                            0 => "iblock_catalog",
                                                        ),
                                                        "CATEGORY_0_iblock_catalog" => array(
                                                            0 => "all",
                                                        ),
                                                        "CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
                                                        "SHOW_INPUT" => "Y",
                                                        "INPUT_ID" => "title-search-input",
                                                        "CONTAINER_ID" => "search",
                                                        "PRICE_CODE" => array(
                                                            0 => "BASE",
                                                        ),
                                                        "SHOW_PREVIEW" => "Y",
                                                        "PREVIEW_WIDTH" => "75",
                                                        "PREVIEW_HEIGHT" => "75",
                                                        "CONVERT_CURRENCY" => "Y"
                                                            ), false
                                                    );
                                                    ?>

                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="menu_line">
                                    <div class="inner_wrapper">
                                        <ul class="menu">
                                            <li><a href="#">����� � ���������� ��������</a></li>
                                            <li><a href="#">�������</a></li>
                                            <li><a href="#">������</a></li>
                                            <li><a href="#">������ � �����</a></li>
                                            <li><a href="#">��������� �����</a></li>
                                            <li><a href="#">����� � ������</a></li>
                                            <li><a href="#">�����������</a></li>
                                            <li><a href="#">������</a></li>
                                            <li><a href="#">����������</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <? if ($APPLICATION->GetCurPage(false) === '/'): ?> 
                                <div class="inner_wrapper">
                                    <div class="soc">
                                        <span>����������</span>
                                        <a href="#" class="s_fb"></a>
                                        <a href="#" class="s_tw"></a>
                                        <a href="#" class="s_vk"></a>
                                    </div>
                                </div>
                                <div class="slider">
                                    <?
                                    $APPLICATION->IncludeComponent("beono:banner_rotation", "banner_rotation", array(
	"SOURCE" => "advert",
	"TYPE" => "main_slider",
	"NOINDEX" => "N",
	"LIMIT" => "5",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "0",
	"JQUERY" => "N",
	"PAGER_STYLE" => "bulls",
	"PAGER_ORIENT" => "horizontal",
	"PAGER_POSITION" => "bottom_center",
	"WIDTH" => "1024px",
	"HEIGHT" => "375px",
	"EFFECT" => "fade",
	"TRANSITION_SPEED" => "300",
	"TRANSITION_INTERVAL" => "5000",
	"STOP_ON_FOCUS" => "N"
	),
	false
);
                                    ?>     

                                    <?
                                    /*$APPLICATION->IncludeComponent("bitrix:news.list", "main_slider", array(
                                        "IBLOCK_TYPE" => "sliders",
                                        "IBLOCK_ID" => "6",
                                        "NEWS_COUNT" => "10",
                                        "SORT_BY1" => "SORT",
                                        "SORT_ORDER1" => "ASC",
                                        "SORT_BY2" => "",
                                        "SORT_ORDER2" => "",
                                        "FILTER_NAME" => "",
                                        "FIELD_CODE" => array(
                                            0 => "",
                                            1 => "",
                                        ),
                                        "PROPERTY_CODE" => array(
                                            0 => "LINK",
                                            1 => "",
                                        ),
                                        "CHECK_DATES" => "Y",
                                        "DETAIL_URL" => "",
                                        "AJAX_MODE" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_FILTER" => "N",
                                        "CACHE_GROUPS" => "Y",
                                        "PREVIEW_TRUNCATE_LEN" => "",
                                        "ACTIVE_DATE_FORMAT" => "",
                                        "SET_STATUS_404" => "N",
                                        "SET_TITLE" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "INCLUDE_SUBSECTIONS" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "�������",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "DISPLAY_DATE" => "N",
                                        "DISPLAY_NAME" => "N",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "N",
                                        "AJAX_OPTION_ADDITIONAL" => ""
                                            ), false
                                    );*/
                                    ?>
                                </div>
                                <div class="inner_wrapper banner">
                                    <?
                                    $APPLICATION->IncludeComponent(
                                            "bitrix:advertising.banner", "", Array(
                                        "TYPE" => "main_page",
                                        "NOINDEX" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "0"
                                            )
                                    );
                                    ?>
                                </div>
                            <? endif ?>
                            <div id="content">
                                <div class="inner_wrapper">





