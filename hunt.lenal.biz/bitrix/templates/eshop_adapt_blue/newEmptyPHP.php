<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/" . SITE_TEMPLATE_ID . "/header.php");
$wizTemplateId = COption::GetOptionString("main", "wizard_template_id", "eshop_adapt_horizontal", SITE_ID);
CUtil::InitJSCore();
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
            <link rel="shortcut icon" type="image/x-icon" href="<?= SITE_DIR ?>/favicon.ico" />
            <?
            //$APPLICATION->ShowHead();
            echo '<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET . '"' . (true ? ' /' : '') . '>' . "\n";
            $APPLICATION->ShowMeta("robots", false, true);
            $APPLICATION->ShowMeta("keywords", false, true);
            $APPLICATION->ShowMeta("description", false, true);
            $APPLICATION->ShowCSS(true, true);
            ?>
            <link rel="stylesheet" type="text/css" href="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH . "/colors.css") ?>" />
            <?
            $APPLICATION->ShowHeadStrings();
            $APPLICATION->ShowHeadScripts();
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/script.js");
            ?>
            
            <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,100&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
            <link href="<?= SITE_TEMPLATE_PATH ?>/css/helpers.css" rel="stylesheet" />
            <link href="<?= SITE_TEMPLATE_PATH ?>/css/styles.css" rel="stylesheet" />
            <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
            <script src="<?= SITE_TEMPLATE_PATH ?>/js/main.js"></script>
            <script src="<?= SITE_TEMPLATE_PATH ?>/js/slider/jquery.bxslider.min.js"></script>
            <link href="<?= SITE_TEMPLATE_PATH ?>/js/slider/jquery.bxslider.css" rel="stylesheet" />

            <script>
                $(document).ready(function(){
                    $('.bxslider').bxSlider({
                        controls : false
                    });

                    $('.brands_slider_body').bxSlider({
                        pager : false,
                        moveSlides: 1,
                        minSlides: 4,
                        maxSlides: 4,
                        slideWidth: 248,
                        infiniteLoop: false

                    });

                    $('.special_offers .items_list, .new_items .items_list, .last_ordered .items_list').bxSlider({
                        minSlides: 4,
                        maxSlides: 4,
                        slideWidth: 240,
                        slideMargin: 23,
                        moveSlides: 1,
                        /*infiniteLoop: false,*/
                        pager: false
                    });
                });
            </script>
            <title><? $APPLICATION->ShowTitle() ?></title>
    </head>
    <body>
        <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>

        <div class="outer_wrapper header">
        <div class="inner_wrapper_min">
            <div id="header">
                <div class="top_logo left w12">
                    <img src="images/logo.png">
                </div>
                <div class="header_inner right">
                    <div class="header_top">
                        <div class="w40 left">
                            <div class="dib top_menu">
                                <ul>
                                    <li><a href="#">������</a></li>
                                    <li><a href="#">��� ������</a></li>
                                    <li><a href="#">� ��������</a></li>
                                    <li><a href="#">��������</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="magazine w20 left">
                            <a href="#">������ <span>&laquo;��� �����&raquo;</span></a>
                        </div>
                        <div class="cabinet w15 left">
                            <a href="#">������ �������</a>
                        </div>
                        <div class="basket left w15">
                            <span class="things_in_basket">2 ������</span>
                            <span>1 450 ���.</span>
                        </div>
                    </div>
                    <div class="header_bottom">
                        <div class="top_contacts left w50">
                            <span class="city">�����-���������</span>
                            <span class="phone"><span class="fOSC fs22">8 (800) 246 55 46</span></span>
                            <a href="#" class="callback">�������� ������</a>
                        </div>
                        <div class="search left w50">
                            <input type="text" name="search" placeholder="�����" />
                            <input type="submit" value="" />
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="menu_line">
            <div class="inner_wrapper">
                <ul class="menu">
                    <li><a href="#">����� � ���������� ��������</a></li>
                    <li><a href="#">�������</a></li>
                    <li><a href="#">������</a></li>
                    <li><a href="#">������ � �����</a></li>
                    <li><a href="#">��������� �����</a></li>
                    <li><a href="#">����� � ������</a></li>
                    <li><a href="#">�����������</a></li>
                    <li><a href="#">������</a></li>
                    <li><a href="#">����������</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="inner_wrapper">
        <div class="soc">
            <span>����������</span>
            <a href="#" class="s_fb"></a>
            <a href="#" class="s_tw"></a>
            <a href="#" class="s_vk"></a>
        </div>
    </div>
    <div class="slider">
        <ul class="bxslider">
            <li><img src="images/slider.jpg" /></li>
            <li><img src="images/slider.jpg" /></li>
        </ul>
    </div>
    <div class="inner_wrapper banner">
        <a href="#"><img src="images/banner.jpg"></a>
    </div>

        <div id="content">
            <div class="inner_wrapper_min">
                <div class="items_block special_offers">
                    <div class="delimiter">
                        <span class="fOSC">����������� �����������</span>
                        <span class="list_item_controls">
                            <div class="dib w50 prev_btn"></div>
                            <div class="dib w50 next_btn"></div>
                        </span>
                    </div>
                    <div class="items_list">
                        <div class="item">
                            <div class="item_top">
                                <a href="#">
                                    <img src="images/item_img.jpg">
                                </a>
                                <div class="item_top_rating">
                                    <span class="stars"></span>
                                    <span class="estimate">203 ������</span>
                                </div>
                            </div>
                            <div class="item_bottom">
                                <div class="price">
                                    <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                    <div class="label_sale">-50%</div>
                                </div>
                                <div class="description">
                                    �����  Invisible hunter trousers
                                </div>
                            </div>
                            <div class="item_label label_spec"></div>
                        </div>
                        <div class="item">
                            <div class="item_top">
                                <a href="#">
                                    <img src="images/item_img.jpg">
                                </a>
                                <div class="item_top_rating">
                                    <span class="stars"></span>
                                    <span class="estimate">203 ������</span>
                                </div>
                            </div>
                            <div class="item_bottom">
                                <div class="price">
                                    <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                    <div class="label_sale">-50%</div>
                                </div>
                                <div class="description">
                                    �����  Invisible hunter trousers
                                </div>
                            </div>
                            <div class="item_label label_spec"></div>
                        </div>
                        <div class="item">
                            <div class="item_top">
                                <a href="#">
                                    <img src="images/item_img.jpg">
                                </a>
                                <div class="item_top_rating">
                                    <span class="stars"></span>
                                    <span class="estimate">203 ������</span>
                                </div>
                            </div>
                            <div class="item_bottom">
                                <div class="price">
                                    <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                    <div class="label_sale">-50%</div>
                                </div>
                                <div class="description">
                                    �����  Invisible hunter trousers
                                </div>
                            </div>
                            <div class="item_label label_spec"></div>
                        </div>
                        <div class="item">
                            <div class="item_top">
                                <a href="#">
                                    <img src="images/item_img.jpg">
                                </a>
                                <div class="item_top_rating">
                                    <span class="stars"></span>
                                    <span class="estimate">203 ������</span>
                                </div>
                            </div>
                            <div class="item_bottom">
                                <div class="price">
                                    <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                    <div class="label_sale">-50%</div>
                                </div>
                                <div class="description">
                                    �����  Invisible hunter trousers
                                </div>
                            </div>
                            <div class="item_label label_spec"></div>
                        </div>
                    </div>
                </div>
                <div class="items_block new_items">
                    <div class="delimiter">
                        <span class="fOSC">�������</span>
                        <span class="list_item_controls">
                            <div class="dib w50 prev_btn"></div>
                            <div class="dib w50 next_btn"></div>
                        </span>
                    </div>
                    <div class="items_list">
                        <div class="item">
                            <div class="item_top">
                                <a href="#">
                                    <img src="images/item_img.jpg">
                                </a>
                                <div class="item_top_rating">
                                    <span class="stars"></span>
                                    <span class="estimate">203 ������</span>
                                </div>
                            </div>
                            <div class="item_bottom">
                                <div class="price">
                                    <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                    <div class="label_sale">-50%</div>
                                </div>
                                <div class="description">
                                    �����  Invisible hunter trousers
                                </div>
                            </div>
                            <div class="item_label label_new"></div>
                            <div class="item_label label_hit"></div>
                        </div>
                        <div class="item">
                            <div class="item_top">
                                <a href="#">
                                    <img src="images/item_img.jpg">
                                </a>
                                <div class="item_top_rating">
                                    <span class="stars"></span>
                                    <span class="estimate">203 ������</span>
                                </div>
                            </div>
                            <div class="item_bottom">
                                <div class="price">
                                    <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                    <div class="label_sale">-50%</div>
                                </div>
                                <div class="description">
                                    �����  Invisible hunter trousers
                                </div>
                            </div>
                            <div class="item_label label_hit"></div>
                            <div class="item_label label_new"></div>
                        </div>
                        <div class="item">
                            <div class="item_top">
                                <a href="#">
                                    <img src="images/item_img.jpg">
                                </a>
                                <div class="item_top_rating">
                                    <span class="stars"></span>
                                    <span class="estimate">203 ������</span>
                                </div>
                            </div>
                            <div class="item_bottom">
                                <div class="price">
                                    <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                    <div class="label_sale">-50%</div>
                                </div>    
                                <div class="description">
                                    �����  Invisible hunter trousers
                                </div>
                            </div>
                            <div class="item_label label_hit"></div>
                        </div>
                        <div class="item">
                            <div class="item_top">
                                <a href="#">
                                    <img src="images/item_img.jpg">
                                </a>
                                <div class="item_top_rating">
                                    <span class="stars"></span>
                                    <span class="estimate">203 ������</span>
                                </div>
                            </div>
                            <div class="item_bottom">
                                <div class="price">
                                    <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                    <div class="label_sale">-50%</div>
                                </div>
                                <div class="description">
                                    �����  Invisible hunter trousers
                                </div>
                            </div>
                            <div class="item_label label_new"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner_wrapper">
                <div class="fat_delimiter"></div>
            </div>
            <div class="inner_wrapper_min">
                <div class="left w75">
                    <div class="news_block">
                        <div class="delimiter">
                            <span class="fOSC">�������</span>
                            <a href="#" class="news_link">��� �������</a>
                        </div>
                        <div class="news_body">
                            <div class="news_el w50 left">
                                <div class="left news_el_img">
                                    <a href="#"><img src="images/news_el.jpg"></a>
                                </div>
                                <div class="right news_el_data">
                                    <p class="news_title"><a href="#">���������� ��������� MINENKO</a></p>
                                    <p class="news_text">������� � ������������ ��������� �� ������� ����������� ������������� ��������� � �������������� MINENKO.</p>
                                    <p class="news_date">21 ����� �14</p>
                                </div>
                            </div>

                        </div>
                        <div class="news_body">
                            <div class="news_el w50 left">
                                <div class="left news_el_img">
                                    <a href="#"><img src="images/news_el.jpg"></a>
                                </div>
                                <div class="right news_el_data">
                                    <p class="news_title"><a href="#">���������� ��������� MINENKO</a></p>
                                    <p class="news_text">������� � ������������ ��������� �� ������� ����������� ������������� ��������� � �������������� MINENKO.</p>
                                    <p class="news_date">21 ����� �14</p>
                                </div>
                            </div>

                        </div>
                        <div class="news_body">
                            <div class="news_el w50 left">
                                <div class="left news_el_img">
                                    <a href="#"><img src="images/news_el.jpg"></a>
                                </div>
                                <div class="right news_el_data">
                                    <p class="news_title"><a href="#">���������� ��������� MINENKO</a></p>
                                    <p class="news_text">������� � ������������ ��������� �� ������� ����������� ������������� ��������� � �������������� MINENKO.</p>
                                    <p class="news_date">21 ����� �14</p>
                                </div>
                            </div>

                        </div>
                        <div class="news_body">
                            <div class="news_el w50 left">
                                <div class="left news_el_img">
                                    <a href="#"><img src="images/news_el.jpg"></a>
                                </div>
                                <div class="right news_el_data">
                                    <p class="news_title"><a href="#">���������� ��������� MINENKO</a></p>
                                    <p class="news_text">������� � ������������ ��������� �� ������� ����������� ������������� ��������� � �������������� MINENKO.</p>
                                    <p class="news_date">21 ����� �14</p>
                                </div>
                            </div>

                        </div>
                        <div class="news_body">
                            <div class="news_el w50 left">
                                <div class="left news_el_img">
                                    <a href="#"><img src="images/news_el.jpg"></a>
                                </div>
                                <div class="right news_el_data">
                                    <p class="news_title"><a href="#">���������� ��������� MINENKO</a></p>
                                    <p class="news_text">������� � ������������ ��������� �� ������� ����������� ������������� ��������� � �������������� MINENKO.</p>
                                    <p class="news_date">21 ����� �14</p>
                                </div>
                            </div>

                        </div>
                        <div class="news_body">
                            <div class="news_el w50 left">
                                <div class="left news_el_img">
                                    <a href="#"><img src="images/news_el.jpg"></a>
                                </div>
                                <div class="right news_el_data">
                                    <p class="news_title"><a href="#">���������� ��������� MINENKO</a></p>
                                    <p class="news_text">������� � ������������ ��������� �� ������� ����������� ������������� ��������� � �������������� MINENKO.</p>
                                    <p class="news_date">21 ����� �14</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="right w23">
                    <div class="experts">
                        <span>������� <br /> � ����������</span>
                        <div class="expert_item">
                            <div class="expert_img left w50">
                                <img src="images/news_el.jpg" />
                            </div>
                            <div class="expert_data left w50">
                                <p class="expert_name">������� �����</p>
                                <p class="expert_prof">C����������� � ���������� ����� �������</p>
                            </div>
                            <div class="clear"></div>
                            <div class="expert_cite">
                                &laquo;<span>�� ������������ ����� ������� ��� ������</span>&raquo;<br />
                                <a href="#">������ ������ �����������</a>
                            </div>
                        </div>
                        <div class="expert_item">
                            <div class="expert_img left w50">
                                <img src="images/news_el.jpg" />
                            </div>
                            <div class="expert_data left w50">
                                <p class="expert_name">������� �����</p>
                                <p class="expert_prof">C����������� � ���������� ����� �������</p>
                            </div>
                            <div class="clear"></div>
                            <div class="expert_cite">
                                &laquo;<span>�� ������������ ����� ������� ��� ������</span>&raquo;<br />
                                <a href="#">������ ������ �����������</a>
                            </div>
                        </div>
                        <p class="all_experts"><a href="#">��� ��������</a></p>
                    </div>
                    <div class="magazine_block">
                        <a href="#"><img src="images/magazine.png"></a>
                        <div class="magazine_block_description">
                            <a href="#">������ ���� ������ �1 �14</a>
                            <span>32,5 mb</span>
                            <div class="clear"></div>
                        </div>
                        <div class="magazine_block_btn">
                            <a class="left" href="#"><img src="images/magazine_pdf.png"></a>
                            <a class="right" href="#"><img src="images/magazine_store.png"></a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>



                    <div class="items_block last_ordered">
                        <div class="delimiter">
                            <span class="fOSC">��������� ���������� ������</span>
                        <span class="list_item_controls">
                            <div class="dib w50 prev_btn"></div>
                            <div class="dib w50 next_btn"></div>
                        </span>
                        </div>
                        <div class="items_list">
                            <div class="item">
                                <div class="item_top">
                                    <a href="#">
                                        <img src="images/item_img.jpg">
                                    </a>
                                    <div class="item_top_rating">
                                        <span class="stars"></span>
                                        <span class="estimate">203 ������</span>
                                    </div>
                                </div>
                                <div class="item_bottom">
                                    <div class="price">
                                        <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                        <div class="label_sale">-50%</div>
                                    </div>
                                    <div class="description">
                                        �����  Invisible hunter trousers
                                    </div>
                                </div>
                                <div class="item_label label_spec"></div>
                            </div>
                            <div class="item">
                                <div class="item_top">
                                    <a href="#">
                                        <img src="images/item_img.jpg">
                                    </a>
                                    <div class="item_top_rating">
                                        <span class="stars"></span>
                                        <span class="estimate">203 ������</span>
                                    </div>
                                </div>
                                <div class="item_bottom">
                                    <div class="price">
                                        <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                        <div class="label_sale">-50%</div>
                                    </div>
                                    <div class="description">
                                        �����  Invisible hunter trousers
                                    </div>
                                </div>
                                <div class="item_label label_spec"></div>
                            </div>
                            <div class="item">
                                <div class="item_top">
                                    <a href="#">
                                        <img src="images/item_img.jpg">
                                    </a>
                                    <div class="item_top_rating">
                                        <span class="stars"></span>
                                        <span class="estimate">203 ������</span>
                                    </div>
                                </div>
                                <div class="item_bottom">
                                    <div class="price">
                                        <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                        <div class="label_sale">-50%</div>
                                    </div>
                                    <div class="description">
                                        �����  Invisible hunter trousers
                                    </div>
                                </div>
                                <div class="item_label label_spec"></div>
                            </div>
                            <div class="item">
                                <div class="item_top">
                                    <a href="#">
                                        <img src="images/item_img.jpg">
                                    </a>
                                    <div class="item_top_rating">
                                        <span class="stars"></span>
                                        <span class="estimate">203 ������</span>
                                    </div>
                                </div>
                                <div class="item_bottom">
                                    <div class="price">
                                        <span class="old_price">26006���.</span> / <span class="new_price">6600 ���</span>
                                        <div class="label_sale">-50%</div>
                                    </div>
                                    <div class="description">
                                        �����  Invisible hunter trousers
                                    </div>
                                </div>
                                <div class="item_label label_spec"></div>
                            </div>
                        </div>
                    </div>
                </div>
        <div class="inner_wrapper">
            <div class="brands_slider">
                <ul class="brands_slider_body">
                    <li><a href="#"><img src="images/prod1.png"></a></li>
                    <li><a href="#"><img src="images/prod2.png"></a></li>
                    <li><a href="#"><img src="images/prod3.png"></a></li>
                    <li><a href="#"><img src="images/prod4.png"></a></li>
                    <li><a href="#"><img src="images/prod1.png"></a></li>
                </ul>
            </div>
        </div>
        <div class="inner_wrapper_min last_order_city">
            <p>��������� ������ �� �������
                <a href="#">���������</a>
                <a href="#">���</a>
                <a href="#">���������</a>
                <a href="#">������</a>
                <a href="#">�����</a>
                <a href="#">������</a>
            </p>
        </div>
        <div class="inner_wrapper_min pb20">
            <div class="about left">
                <div class="big_title">� ��������</div>
                <div class="text">
                    <p>���� ��������� ���� ������ - ���� �� ���������� �������� ����� ������� ��� ��������� ������ � ������.</p><br />
                    <p>���� ��������� ���� ������ ���� �������� � 1999 ����. �������� ������ ���������� ��������� ��������� ������������� ��� ����������� ������������� � ������ ���������� ���������. ���������� �������� �������� �������� ����������, ����������, ��������� � ������ ������� ������ �� ������ � ������ ������������ ��� ��� ������ � ������ ������� ���������� �����.</p><br />
                    <p>��������� ���������: ����, ������, ���������, �����-���������, ���������, ������-��-����, �������, �����������, ���������, ������, �������, ������������, ����, ��������, ������, ���.</p>
                </div>
            </div>
            <div class="tabs left">
                vk
            </div>
            <div class="clear"></div>
        </div>


        </div>

        <div id="footer">
            <div class="inner_wrapper_min">
                <div class="footer_menu">
                    <div class="dib">
                        <p>�������</p>
                        <ul>
                            <li><a href="#">����� � ���������� ��������</a></li>
                            <li><a href="#">�������</a></li>
                            <li><a href="#">������</a></li>
                            <li><a href="#">������ � �����</a></li>
                            <li><a href="#">��������� ����� </a></li>
                            <li><a href="#">����� � ������</a></li>
                            <li><a href="#">����������� </a></li>
                            <li><a href="#">������</a></li>
                            <li><a href="#">��� ��������� �����</a></li>
                            <li><a href="#">������� ��������������</a></li>
                            <li><a href="#">����������</a></li>
                        </ul>
                    </div>
                    <div class="dib">
                        <p>� ��������</p>
                        <ul>
                            <li><a href="#">�������</a></li>
                            <li><a href="#">���� ��������</a></li>
                            <li><a href="#">�������</a></li>
                            <li><a href="#">�����������</a></li>
                            <li><a href="#">������������ ������������</a></li>
                            <li><a href="#">���� �������</a></li>
                            <li><a href="#">������ ���� ������</a></li>
                            <li><a href="#">�����������</a></li>
                            <li><a href="#">�����������</a></li>
                            <li><a href="#">��������</a></li>
                            <li><a href="#">��������</a></li>
                        </ul>
                    </div>
                    <div class="dib">
                        <p>������</p>
                        <ul>
                            <li><a href="#">��� ������</a></li>
                            <li><a href="#">��������</a></li>
                            <li><a href="#">������</a></li>
                            <li><a href="#">�������-������</a></li>
                            <li><a href="#">������� � �����</a></li>
                            <li><a href="#">�������</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">�������� ����������</a></li>
                        </ul>
                    </div>
                    <div class="dib">
                        <p>��� ������</p>
                        <ul>
                            <li><a href="#">���������� ���������</a></li>
                            <li><a href="#">���������� �����</a></li>
                            <li><a href="#">���� � ������</a></li>
                            <li><a href="#">����� ��� �����</a></li>
                            <li><a href="#">������� � ����������</a></li>
                        </ul>
                    </div>
                    <div class="dib">
                        <p>������ �������</p>
                        <ul>
                            <li><a href="#">��������� �������</a></li>
                            <li><a href="#">�������</a></li>
                            <li><a href="#">������� �������</a></li>
                            <li><a href="#">Wish list</a></li>
                            <li><a href="#">����� � ������</a></li>
                            <li><a href="#">��� ������</a></li>
                            <li><a href="#">��� ��������</a></li>
                        </ul>
                    </div>
                    <div class="dib">
                        <p>��������</p>
                        <ul>
                            <li><a href="#">���</a></li>
                            <li><a href="#">���������</a></li>
                            <li><a href="#">������</a></li>
                            <li><a href="#">����</a></li>
                            <li><a href="#">������-��-����</a></li>
                            <li><a href="#">�����-���������</a></li>
                            <li><a href="#">���������</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">
            <div class="inner_wrapper_min">
                <div class="footer_bottom_fline">
                    <div class="fline_left left w50">
                        <div class="footer_shared dib left">
                            <a href="#" class="fb"></a>
                            <a href="#" class="tw"></a>
                            <a href="#" class="yt"></a>
                            <a href="#" class="rss"></a>
                        </div>
                        <div class="subscribe dib right">
                            <input type="text" name="subscribe" placeholder="@ E-mail" />
                            <input type="submit" value="�����������" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="fline_right right w50 tar">
                        <div class="order_status">
                            <input type="text" name="order_status" placeholder="������ ������" />
                            <input type="submit" value="" />
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="footer_bottom_sline">
                    <div class="text_line left w50">
                        <span class="left">� 2005-2014 ��� ���� ������ ���: 2312076658</span>
                        <span class="right">������� ������? <a href="#">�������� ���</a></span>
                        <span class="clear"></span>
                        <p>������ ��� ������</p>
                    </div>
                    <div class="pay_systems right w50 tar">
                        <a href="#"><img src="images/ps1.png"></a>
                        <a href="#"><img src="images/ps2.png"></a>
                        <a href="#"><img src="images/ps3.png"></a>
                        <a href="#"><img src="images/ps4.png"></a>
                        <a href="#"><img src="images/ps5.png"></a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <div class="wrap" id="bx_eshop_wrap">
            <div class="header_wrap">
                <div class="header_wrap_container">
                    <div class="header_top_section">
                        <div class="header_top_section_container_one">

                            <div class="bx_cart_login_top">

                                <table>
                                    <tr>
                                        <td>
                                            <?
                                            $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", ".default", array(
	"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
	"PATH_TO_PERSONAL" => SITE_DIR."personal/",
	"SHOW_PERSONAL_LINK" => "N",
	"SHOW_NUM_PRODUCTS" => "Y",
	"SHOW_TOTAL_PRICE" => "Y",
	"SHOW_PRODUCTS" => "Y",
	"POSITION_FIXED" => "N",
	"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
	"SHOW_DELAY" => "Y",
	"SHOW_NOTAVAIL" => "Y",
	"SHOW_SUBSCRIBE" => "Y",
	"SHOW_IMAGE" => "Y",
	"SHOW_PRICE" => "Y",
	"SHOW_SUMMARY" => "Y"
	),
	false
);
                                            ?>
                                        </td>
                                        <td>

                                            <?
                                            $APPLICATION->IncludeComponent("bitrix:system.auth.form", "eshop_adapt", array(
                                                "REGISTER_URL" => SITE_DIR . "login/",
                                                "PROFILE_URL" => SITE_DIR . "personal/",
                                                "SHOW_ERRORS" => "N"
                                                    ), false, array()
                                            );
                                            ?>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="header_top_section_container_two">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/logo.php"), false); ?>

                            <?
                            $APPLICATION->IncludeComponent('bitrix:menu', "top_menu", array(
                                "ROOT_MENU_TYPE" => "top",
                                "MENU_CACHE_TYPE" => "Y",
                                "MENU_CACHE_TIME" => "36000000",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "MENU_CACHE_GET_VARS" => array(),
                                "MAX_LEVEL" => "1",
                                "USE_EXT" => "N",
                                "ALLOW_MULTI_SELECT" => "N"
                                    )
                            );
                            ?>
                            <a href="javascript:void(0)" class="callback">�������� ������</a>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/magazine.php"), false); ?>

                        </div>
                        <div class="clb"></div>
                    </div>  <!-- //header_top_section -->

                    <div class="header_inner" itemscope itemtype = "http://schema.org/LocalBusiness">

                        <?
                        $APPLICATION->IncludeComponent("lenal:city.list", ".default", array(
                            "IBLOCK_TYPE" => "city_list",
                            "IBLOCK_ID" => "4",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_GROUPS" => "Y"
                                ), false
                        );
                        ?>

                        <div class="header_inner_container_one">
                            <div class="header_inner_include_aria"><span style="color: #1b5c79;">
                                    <strong style="display: inline-block;padding-top: 7px;"><a style="text-decoration: none;color:#1b5c79;" href="<?= SITE_DIR ?>about/contacts/" itemprop = "telephone"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/telephone.php"), false); ?></a></strong><br />
                                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/schedule.php"), false); ?></span>
                            </div>
                        </div>

                        <div class="header_inner_container_two">
                            <?
                            $APPLICATION->IncludeComponent("bitrix:search.title", "visual", array(
                                "NUM_CATEGORIES" => "1",
                                "TOP_COUNT" => "5",
                                "CHECK_DATES" => "N",
                                "SHOW_OTHERS" => "N",
                                "PAGE" => SITE_DIR . "catalog/",
                                "CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
                                "CATEGORY_0" => array(
                                    0 => "iblock_catalog",
                                ),
                                "CATEGORY_0_iblock_catalog" => array(
                                    0 => "all",
                                ),
                                "CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
                                "SHOW_INPUT" => "Y",
                                "INPUT_ID" => "title-search-input",
                                "CONTAINER_ID" => "search",
                                "PRICE_CODE" => array(
                                    0 => "BASE",
                                ),
                                "SHOW_PREVIEW" => "Y",
                                "PREVIEW_WIDTH" => "75",
                                "PREVIEW_HEIGHT" => "75",
                                "CONVERT_CURRENCY" => "Y"
                                    ), false
                            );
                            ?>
                        </div>

                        <div class="clb"></div>
                        <div class="header_inner_bottom_line_container">
                            <div class="header_inner_bottom_line">
                                <? if ($wizTemplateId == "eshop_adapt_horizontal"): ?>
                                    <?
                                    $APPLICATION->IncludeComponent("bitrix:menu", "catalog_horizontal", array(
                                        "ROOT_MENU_TYPE" => "left",
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_TIME" => "36000000",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_THEME" => "site",
                                        "CACHE_SELECTED_ITEMS" => "N",
                                        "MENU_CACHE_GET_VARS" => array(
                                        ),
                                        "MAX_LEVEL" => "3",
                                        "CHILD_MENU_TYPE" => "left",
                                        "USE_EXT" => "Y",
                                        "DELAY" => "N",
                                        "ALLOW_MULTI_SELECT" => "N",
                                            ), false
                                    );
                                    ?>
                                <? endif ?>
                            </div>
                        </div><!-- //header_inner_bottom_line_container -->

                    </div>  <!-- //header_inner -->
                    <? if ($APPLICATION->GetCurPage(true) == SITE_DIR . "index.php"): ?>
                        <?
                        $APPLICATION->IncludeComponent("bitrix:news.list", "main_slider", array(
                            "IBLOCK_TYPE" => "sliders",
                            "IBLOCK_ID" => "6",
                            "NEWS_COUNT" => "10",
                            "SORT_BY1" => "SORT",
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => "",
                            "FIELD_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "PROPERTY_CODE" => array(
                                0 => "LINK",
                                1 => "",
                            ),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "INCLUDE_SUBSECTIONS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "�������",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "N",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_OPTION_ADDITIONAL" => ""
                                ), false
                        );
                        ?>
                        <?
                        $APPLICATION->IncludeComponent(
                                "bitrix:advertising.banner", "", Array(
                            "TYPE" => "main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                                )
                        );
                        ?>
                    <? endif ?>

                </div> <!-- //header_wrap_container -->
            </div> <!-- //header_wrap -->

            <div class="workarea_wrap">
                <div class="worakarea_wrap_container workarea <? if ($wizTemplateId == "eshop_adapt_vertical"): ?>grid1x3<? else: ?>grid<? endif ?>">


                    <div id="navigation">
                        <?
                        $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                            "START_FROM" => "0",
                            "PATH" => "",
                            "SITE_ID" => "-"
                                ), false, Array('HIDE_ICONS' => 'Y')
                        );
                        ?>
                    </div>
                    <div class="bx_content_section">
                        <? if ($curPage != SITE_DIR . "index.php"): ?>
                            <h1><?= $APPLICATION->ShowTitle(false); ?></h1>
                            <?
                        
                        
                        
                        
                        
                        
endif?>
