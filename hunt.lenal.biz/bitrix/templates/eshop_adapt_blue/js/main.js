$(function(){
    $('#content .delimiter span.list_item_controls div.next_btn').live('click',function(){
        $(this).closest('.items_block').find('.bx-next').click();
    });
    $('#content .delimiter span.list_item_controls div.prev_btn').live('click',function(){
        $(this).closest('.items_block').find('.bx-prev').click();
    });

    /* tabs */
    tabs('.tabs');

    $('.tabs span').click(function(){
        $(this).closest('.tabs').find('span').removeClass('active');
        $(this).closest('.tabs').find('div.tabs_content').find('div').removeClass('active');
        $(this).addClass('active');
        $(this).closest('.tabs').find('div.tabs_content').find('div[class='+$(this).attr('id')+']').addClass('active');
        tabs('.tabs');
    });

    /* small cart price && deactivate dec. btn */
    var resPrice = 0;
    var sumCnt = 0;
    $('.basket_popup').find('.count_dec span').each(function(i, el){
        var elCnt = +$(el).closest('.basket_count').find('.count_val > span').text();
        sumCnt += elCnt;
        resPrice += $(el).data('price') * elCnt;
    });
    $('.result_price, .things_price').text(resPrice);
    $('.things_in_basket > span').text(sumCnt);
    $(".ending").text(pluralForm(sumCnt));


    /* small basket show */
    $('.basket').hover(function(){
            $('.basket_popup').stop(false, true).fadeIn(300);
            $('.basket_popup').find('.count_dec span').each(function(i, el){
                //resPrice += $(el).data('price');
                if(+$(el).closest('.basket_count').find('.count_val > span').text() < 2){
                    $(el).addClass('min');
                }
            });
        },
        function(){
            $('.basket_popup').stop(false, true).fadeOut(300);
    });

    /* small basket buy */
    $('.count_inc span').click(function(){
        var cnt = $(this).closest('.basket_count').find('.count_val > span');
        var price = $(this).data('price');
        var id = $(this).data('id');
        cnt.text(+cnt.text()+1);
        cl(cnt.text()+' | '+price+' | '+id+' | ');
        $('.result_price, .things_price').text(+$('.result_price').text() + (+price));
        $('.things_in_basket > span').text(+$('.things_in_basket > span').text() + 1);
        
        
        $(".ending").text(pluralForm(+$('.things_in_basket > span').text() +1));
        
        
        $(this).closest('.basket_count').find('.count_dec span').removeClass('min');
cl(id);
        $.post('/ajax/services/small_basket.php',{'id':id,'quantity':cnt.text()});
    });

    $('.count_dec span:not(.min)').click(function(){
        var cnt = $(this).closest('.basket_count').find('.count_val > span');
        var id = $(this).data('id');
        var price = $(this).data('price');
        if(+cnt.text() <= 1 ){
            cnt.text(1);
            $(this).addClass('min');
        }
        else{
    
            if(+cnt.text()-1 == 1){
                $(this).addClass('min');
            }
            cnt.text(+cnt.text()-1);
            $('.result_price, .things_price').text(+$('.things_price').text() - (+price));
            
            $('.things_in_basket > span').text(+$('.things_in_basket > span').text() - 1);
            $.post('/ajax/services/small_basket.php',{'id':id,'quantity':cnt.text()});
            
            $(".ending").text(pluralForm(+$('.things_in_basket > span').text()));

        }
    });

    /* small basket del */
    $('.basket_del').click(function(){
        $(this).closest('.basket_line').fadeOut();
        var id = $(this).closest('.basket_line').find('.count_dec span').data('id');
        var price = $(this).closest('.basket_line').find('.count_dec span').data('price');
        var cnt = +$(this).closest('.basket_line').find('.count_val > span').text();

        $('.result_price, .things_price').text(+$('.things_price').text() - ((+price) * cnt));
        $('.things_in_basket > span').text(+$('.things_in_basket > span').text() - cnt);

        $.post('/ajax/services/small_basket.php',{'id':id,'quantity':0});
        
        $(".ending").text(pluralForm(+$('.things_in_basket > span').text()));
    });
});


function tabs(selector){
    var active = $(selector).find('span.active').attr('id');
    $(selector).find('div.tabs_content > div.'+active).addClass('active');
}

function cl(data){
    console.log(data);
}

function pluralForm(iNumber){
    
    
     var sEnding, i;
    iNumber = iNumber % 100;
    if (iNumber>=11 && iNumber<=19) {
        sEnding='������';
    }
    else {
        i = iNumber % 10;
        switch (i)
        {
            case (1): sEnding = '�����'; break;
            case (2):
            case (3):
            case (4): sEnding = '������'; break;
            default: sEnding = '�������';
        }
    }
    return sEnding;
}