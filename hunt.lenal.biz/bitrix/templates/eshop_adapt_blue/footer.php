<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
  </div> </div>
<div id="footer">
    <div class="inner_wrapper_min">
        <div class="footer_menu">
            <div class="dib">
                <p>�������</p>
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section.list", "menu_sections_bottom", Array(
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => "2",
                    "SECTION_ID" => $_REQUEST["SECTION_ID"],
                    "SECTION_CODE" => "",
                    "COUNT_ELEMENTS" => "N",
                    "TOP_DEPTH" => "1",
                    "SECTION_FIELDS" => array("", ""),
                    "SECTION_USER_FIELDS" => array("", ""),
                    "VIEW_MODE" => "TEXT",
                    "SHOW_PARENT_NAME" => "Y",
                    "SECTION_URL" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "ADD_SECTIONS_CHAIN" => "Y"
                        )
                );
                ?>
            </div>
            <div class="dib">
                <p>� ��������</p>
                <?
                $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
                    "ROOT_MENU_TYPE" => "bottom_company",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                        ), false
                );
                ?>
            </div>
            <div class="dib">
                <p>������</p>
                <?
                $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
                    "ROOT_MENU_TYPE" => "bottom_help",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                        ), false
                );
                ?>
            </div>
            <div class="dib">
                <p>��� ������</p>
                <?
                $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
                    "ROOT_MENU_TYPE" => "bottom_service",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                        ), false
                );
                ?>
            </div>
            <div class="dib">
                <p>������ �������</p>
                <?
                $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
                    "ROOT_MENU_TYPE" => "bottom_personal",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                        ), false
                );
                ?>
            </div>
            <div class="dib">
                <p>��������</p>
                <?
                $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
                    "ROOT_MENU_TYPE" => "bottom_shops",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                        ), false
                );
                ?>
            </div>
        </div>
    </div>
</div>
<div class="footer_bottom">
    <div class="inner_wrapper_min">
        <div class="footer_bottom_fline">
            <div class="fline_left left w50">
                <div class="footer_shared dib left">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/socials.php"), false); ?>



                </div>
                <div class="subscribe dib right">
                    <?
                    $APPLICATION->IncludeComponent("bitrix:subscribe.form", ".default", array(
                        "USE_PERSONALIZATION" => "Y",
                        "SHOW_HIDDEN" => "N",
                        "PAGE" => "#SITE_DIR#personal/subscribe/",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600"
                            ), false
                    );
                    ?>
                </div>
                <div class="clear"></div>
            </div>
            <div class="fline_right right w50 tar">
                <div class="order_status">
                    <input type="text" name="order_status" id="order_id" placeholder="������ ������"> <input type="submit" value="" id="order_state_check"/>
                    <div class="order_state_result"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="footer_bottom_sline">
            <div class="text_line left w50">
                <span class="left">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/copyright.php"), false); ?>
                </span>
                <span class="right">������� ������? <a href="mailto:info@sale.com">�������� ���</a></span>
                <span class="clear"></span>
                <p>������ ��� ������</p>
            </div>
            <div class="pay_systems right w50 tar">
                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/payments.php"), false); ?>

            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div id="myModal" class="reveal-modal">
    <div class="callback_popup">
        <input type = "text" value="" id="phone_value" placeholder="+7 (___) ___ __ __"/>
        <a class="callback_submit" href="javascript:void(0)">���������</a>
    </div>
</div>
 
<div id="city_popup" class="reveal-modal">
    <?$APPLICATION->ShowViewContent("city_list_popup");?>   
</div>  
  
  
</body>
</html>