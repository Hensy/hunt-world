function eshopOpenNativeMenu()
{
    var native_menu = BX("bx_native_menu");
    var is_menu_active = BX.hasClass(native_menu, "active");

    if (is_menu_active)
    {
        BX.removeClass(native_menu, "active");
        BX.removeClass(BX('bx_menu_bg'), "active");
        BX("bx_eshop_wrap").style.position = "";
        BX("bx_eshop_wrap").style.top = "";
        BX("bx_eshop_wrap").style.overflow = "";
    }
    else
    {
        BX.addClass(native_menu, "active");
        BX.addClass(BX('bx_menu_bg'), "active");
        var topHeight = document.body.scrollTop;
        BX("bx_eshop_wrap").style.position = "fixed";
        BX("bx_eshop_wrap").style.top = -topHeight + "px";
        BX("bx_eshop_wrap").style.overflow = "hidden";
    }

    var easing = new BX.easing({
        duration: 300,
        start: {left: (is_menu_active) ? 0 : -100},
        finish: {left: (is_menu_active) ? -100 : 0},
        transition: BX.easing.transitions.quart,
        step: function(state) {
            native_menu.style.left = state.left + "%";
        }
    });
    easing.animate();
}

window.addEventListener('resize',
        function() {
            if (window.innerWidth >= 640 && BX.hasClass(BX("bx_native_menu"), "active"))
                eshopOpenNativeMenu();
        },
        false
        );

$(document).ready(function()
{
    
    $(".callback").click(function()
    {
        $(".callback_popup").toggle("fast");
        
    });
    

    $('.bxslider').bxSlider({
        controls: false,
        slideWidth: 1024
    });

    $('.brands_slider_body').bxSlider({
        pager: false,
        moveSlides: 1,
        minSlides: 5,
        maxSlides: 5,
        slideWidth: 248,
        infiniteLoop: false

    });

    $('.special_offers .items_list, .new_items .items_list, .last_ordered .items_list').bxSlider({
        minSlides: 4,
        maxSlides: 4,
        slideWidth: 230,
        slideMargin: 23,
        moveSlides: 1,
        infiniteLoop: true,   
        pager: false
    });




$("#phone_value").focus(function()
{
    $("#phone_value").val("");
});

$("#order_state_check").click(function()
{
    var order_id = $("#order_id").val();
    if (order_id === "")
    {
        $("#order_id").css("border", "1px solid red");
        $("#order_id").val("���� �� ���������");
    }
    else
    {
        $.ajax({
            type: "POST",
            url: "/ajax/services/check_order.php",
            dataType: "html",
            data: {order_id: order_id},
            success: function(html)
            {
                $(".order_state_result").html(html);
            }
        });
    }
});

$(".callback_submit").click(function()
{
    var phone = $("#phone_value").val();
    var check_express = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
    if (!check_express.test(phone))
    {
        $("#phone_value").css("border", "1px solid red");
        $("#phone_value").val("���� �� ���������");

    }
    else
    {
        $.ajax({
            type: "POST",
            url: "/ajax/services/callback.php",
            dataType: "html",
            data: {phone: phone},
            success: function(data)
            {
                $(".callback_submit").html(data);
            }
        });
    }
});


$(".item_top_rating").hover(function()
{
    $(this).find(".show_rating").stop(true,false).fadeIn("fast");
},function()
{
    $(this).find(".show_rating").stop(true,false).fadeOut("fast");
});
        });
        

function addToCompare(element, mode, text, deleteUrl) {
    if (!element && !element.href)
        return;
    var href = element.href;
    var button = $(element);
    button/*.unbind('click')*/.removeAttr("href");
    titleItem = $(element).parents(".bx_catalog_item").find(".text_desc a").text();
    imgItem = $(element).parents(".bx_catalog_item").find(".item-img").find("img").attr('src');
    console.log(imgItem);
    console.log(titleItem);
    if ((imgItem == undefined) && (titleItem == undefined) || (imgItem == undefined) && (titleItem == '')) {
        titleItem = $(".bx_item_detail h2 span").html();
        imgItem = $(".bx_bigimages_imgcontainer img").attr('src');
    }
    console.log(imgItem);
//    $('#addItemInCompare p').prepend(titleItem);
//    $('#addItemInCompare img.item_img').attr('src', imgItem);
    var li = "<li><div><img class='item_img' alt='' src='"+ imgItem +"'></div><p>"+ titleItem +"<br><span></span></p><a class='remove' style='cursor: pointer; background: url(/bitrix/templates/eshop_adapt_blue/images/modwin/remove.png); width:11px; height:10px;'></a></li>";
    $('#addItemInCompare ul').prepend(li);

    var ModalName = $('#addItemInCompare');
    $('.modalWrap').show();
   // CentriredModalWindow(ModalName);
   // OpenModalWindow(ModalName);
//$('#addItemInCompare .modal_middle__h1').html("����� �������� � ���������");
//$(".modal_middle__btn-go_big").attr('href', '/catalog/compare/').text('������ ���������').attr('title', '������� � ������ ���������');
    if (mode == 'list')
    {
        var removeCompare = '<input type="checkbox" class="addtoCompareCheckbox"/ checked><span class="checkbox_text">' + text + '</span>';
        button.html(removeCompare);
        button.attr("href", deleteUrl);
        button.attr("onclick", "return deleteFromCompare(this, \'" + mode + "\', \'" + text + "\', \'" + href + "\');");
//button.attr("onclick", "return deleteFromCompare(this, \'" + mode + "\', \'�������\', \'" + href + "\');");
    }
    else if (mode == 'detail' || mode == 'list_price')
    {
        var removeCompare = text;
//button.html(removeCompare);
    }
    if (href)
        $.get(href + '&ajax_compare=1&backurl=' + decodeURIComponent(window.location.pathname)
                );
    return false;
}
function deleteFromCompare(element, mode, text, compareUrl) {
    if (!element || !element.href)
        return;
//var href = element.href;
    var button = $(element);
    var href = button.attr("href");
    button.unbind('click').removeAttr("href");
    if (mode == 'list')
    {
        var removeCompare = '<input type="checkbox" class="addtoCompareCheckbox"/><span class="checkbox_text">' + text + '</span>';
        button.html(removeCompare);
        button.attr("href", compareUrl);
        button.attr("onclick", "return addToCompare(this, \'" + mode + "\', \'" + text + "\', \'" + href + "\');");
    }
    $.get(href + '&ajax_compare=1&backurl=' + decodeURIComponent(window.location.pathname)
            );
    return false;
}
function deleteFromCompareTable(element) {
    var counttd = $(".table_compare table tr:first").find("td img").size()
    if (counttd == 1) {
        var heightsum = $(".workarea").height() - $(".breadcrumbs").height()
        $(".table_compare").remove();
        $(".filtren.compare").remove();
        $(".deleteAllFromCompareLink").remove();
        $(".emptyListCompare").css("display", "block");
        $(".sort").remove();
        $(".nocomapre").css({"height": heightsum + "px", "display": "table", "width": 100 + "%"}, 300);
    } else if (counttd < 4) {
        compareTable(counttd);
    }
    var tdInd = $(element).parent('td').index();
    wtdc = $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").width()
    $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").css({"width": wtdc + "px", "padding": 0 + "px"});
    $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").animate({"width": 0 + "px", "padding": 0 + "px"}, 300);
    $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").text('');
    $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").remove();
    var href = element.href;
    $.get(href);
    return false;
}        
        
