<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<span class="city" data-reveal-id="city_popup"><?
    if (CModule::IncludeModule("altasib.geoip")) {
        $arData = ALX_GeoIP::GetAddr();
    }
    if (empty($_SESSION["city_name"])) {
        print_r($arData["city"]);
    } else {
        echo ($_SESSION["city_name"]);
    }
    ?>
</span>


<span class="phone">
    <? 
    $city_array = array();
    foreach ($arResult as $arItem): ?>
    
        <?$city_array[] = $arItem["NAME"];?>
        <span class="fOSC fs22 city_<?=$arItem["ID"]?>"   <?if($_SESSION["city_name"]==$arItem["NAME"]) echo "style='display:inline'"?>>
           <?=$arItem["PROPERTY_CITY_PHONE_VALUE"]?>
        </span>
    <? endforeach ?>
</span>

<!--���� ������ ��� � ������� �� ���������, ������� �� ���������� �������-->
<?if(!in_array($_SESSION["city_name"],$city_array))
{?>
<span class="phone">
 <span class="fOSC fs22" style="display:inline">
     <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/telephone.php"), false); ?>
 </span>
</span>
<?}?>

<? $this->SetViewTarget("city_list_popup"); ?>
<ul class="city_list">
    <? foreach ($arResult as $arItem): ?>
        <li><a href="javascript:void(0)" onclick="saveLocation('<?= $arItem["NAME"] ?>', '<?= $templateFolder ?>',<?=$arItem["ID"]?>)"><?= $arItem["NAME"] ?></a></li>
    <? endforeach; ?>
</ul>
<? $this->EndViewTarget(); ?>

<script>

    $(".city").click(function()
    {
        $('#city_popup').reveal({
            animation: 'fadeAndPop', //fade, fadeAndPop, none
            animationspeed: 300, //how fast animtions are
            closeonbackgroundclick: true, //if you click background will modal close?
            dismissmodalclass: 'close-reveal-modal'    //the class of a button or element that will close an open modal
        });
        
    });
    function saveLocation(city_name, template_path,city_id)
    {
        
        $(".fOSC.fs22").css("display","none");
        $(".city_"+city_id).css("display","inline");
        //save for Javascript
        localStorage.setItem("city", city_name);


        //set SESSION 
        $.ajax({
            type: "POST",
            url: template_path + "/ajax.php",
            data: {city_name: city_name},
            success: function(html)
            {
                $(".city").text(html);
            }
        });

    }
</script>    



