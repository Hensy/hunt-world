<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?><br />
<? endif; ?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="expert_item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">



        <div class="expert_img left w50">
            <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                  
            <? $file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>103, 'height'=>113), BX_RESIZE_IMAGE_EXACT, true); ?>
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img
                            class="preview_picture"
                            border="0"
                            src="<?= $file["src"] ?>"
                            width="<?= $file["width"] ?>"
                            height="<?= $file["height"] ?>"
                            alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                            title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                            style="float:left"
                            /></a>
                    <? else: ?>
                    <img
                        class="preview_picture"
                        border="0"
                        src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                        width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>"
                        height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>"
                        alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                        title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                        style="float:left"
                        />
                    <? endif; ?>
                <? endif ?>
        </div>   
        <div class="expert_data left w50">
            <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                    <p class="expert_name"><a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a></p>
                <? else: ?>
                    <b><? echo $arItem["NAME"] ?></b><br />
                <? endif; ?>
            <? endif; ?>

            <? if ($arItem["PROPERTIES"]["SPECIALIZATION"]["VALUE"]!=""): ?>
                <p class="expert_prof"><? echo $arItem["PROPERTIES"]["SPECIALIZATION"]["VALUE"]; ?></p>
            <? endif; ?>
        </div>                
        <div class="clear"></div>


        <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
            <div class="expert_cite">&laquo;<span><? echo $arItem["PREVIEW_TEXT"]; ?></span>&raquo;<br />
                <a href="#">������ ������ �����������</a>
            </div>   
        <? endif; ?>


    </div>
<? endforeach; ?>
<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <br /><?= $arResult["NAV_STRING"] ?>
<? endif; ?>

