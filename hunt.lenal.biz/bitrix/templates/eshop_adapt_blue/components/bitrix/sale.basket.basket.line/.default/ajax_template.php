<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
$this->IncludeLangFile('template.php');
if (!function_exists('sbblSmallCartTemplate')) {

    function sbblSmallCartTemplate($arResult, $arParams) {
        ?>



        <? if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y'): ?>
            <? if ($arResult['NUM_PRODUCTS'] > 0): ?>
                <a href="<?= $arParams['PATH_TO_BASKET'] ?>" ><span class="things_in_basket"><span><?  '</span> ' . $arResult['PRODUCT(S)'] ?></span> <div class="ending"></div></a>
            <? else: ?>
                <span class="things_in_basket"><span><? '</span> ' . $arResult['PRODUCT(S)'] ?></span><div class="ending"></div>
            <? endif ?>
        <? endif ?>

        <? if ($arParams['SHOW_TOTAL_PRICE'] == 'Y'): ?>
            <?$price_without_cur = explode(" ",$arResult['TOTAL_PRICE']);?>    

            <? if ($arResult['NUM_PRODUCTS'] > 0 && $arParams['SHOW_NUM_PRODUCTS'] == 'N'): ?>
                <a href="<?= $arParams['PATH_TO_BASKET'] ?>"> <span><?= $price_without_cur[0] ?></span></a>
            <? else: ?>
                <span class="things_price"><span><?= $price_without_cur[0] ?></span> </span> ���.
            <? endif ?>
        <? endif ?>

        <? if ($arParams['SHOW_PERSONAL_LINK'] == 'Y'): ?>
            <a class="link_profile" href="<?= $arParams['PATH_TO_PERSONAL'] ?>"><?= GetMessage('TSB1_PERSONAL') ?></a>
        <? endif ?>






        <?
    }

}
?>
<? if ($arParams["SHOW_PRODUCTS"] == "N"): ?>
    <? sbblSmallCartTemplate($arResult, $arParams) ?>
<? else: ?>

    <? if ($arParams["POSITION_FIXED"] == "Y"): ?>
        <? sbblSmallCartTemplate($arResult, $arParams) ?>
    <? else: ?>

    <? endif ?>






    <? if ($arResult['NUM_PRODUCTS'] > 0): ?>

        <div class="basket_popup">

            <? if ($arParams["POSITION_FIXED"] == "Y"): ?>
                <div id="bx_cart_block_status" class="status" onclick="sbbl.toggleExpandCollapseCart()"><?= GetMessage("TSB1_EXPAND") ?></div>
                <? endif ?>

            <div id="bx_cart_block_products" class="basket_table">
                <?
                foreach ($arResult["CATEGORIES"] as $category => $items):
                    if (empty($items))
                        continue;
                    ?>
            <? foreach ($items as $v): ?>
                        <div class="basket_line">
                            <div class="bx_item_delete" onclick="sbbl.removeItemFromCart(<?= $v['ID'] ?>)" title="<?= GetMessage("TSB1_DELETE") ?>"></div>
                            
                                <? if ($arParams["SHOW_IMAGE"] == "Y"): ?>
                                <div class="basket_img">
                                    <? if ($v["PICTURE_SRC"]): ?>
                                        <? if ($v["DETAIL_PAGE_URL"]): ?>
                                            <a href="<?= $v["DETAIL_PAGE_URL"] ?>"><img src="<?= $v["PICTURE_SRC"] ?>" alt="<?= $v["NAME"] ?>"></a>
                                        <? else: ?>
                                            <img src="<?= $v["PICTURE_SRC"] ?>" alt="<?= $v["NAME"] ?>" />
                                    <? endif ?>
                                    <? endif ?>
                                </div>
                                <? endif ?>
                            <div class="basket_data">
                                <? if ($v["DETAIL_PAGE_URL"]): ?>
                                    <p><a href="<?= $v["DETAIL_PAGE_URL"] ?>"><?= $v["NAME"] ?></a></p>
                            <? else: ?>
                                <?= $v["NAME"] ?>
                            <? endif ?>
                           
                                <? if (true):/* $category != "SUBSCRIBE") TODO */ ?>
                                    <? if ($arParams["SHOW_PRICE"] == "Y"): ?>
                                    <p class="bx_item_price">
                                        <?= $v["PRICE_FMT"] ?>
                                    <? //if ($v["FULL_PRICE"] != $v["PRICE_FMT"]): ?>
                                            <!--<span class="bx_item_oldprice"><?= $v["FULL_PRICE"] ?></span>-->
                                    <?// endif ?>
                                    </p>
                            </div>  
                            <div class="basket_count">
                                <p>���-��</p>
                                <div>
                                    <div class="left w26 count_dec"><span data-id="<?= $v['ID'] ?>" data-price="<?= round($v["PRICE"]) ?>">-</span></div>
                                    <div class="left w30 count_val"><span><?= $v["QUANTITY"] ?></span></div>
                                    <div class="left w45 count_inc"><span data-id="<?= $v['ID'] ?>" data-price="<?= round($v["PRICE"]) ?>">+</span> ��.</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="basket_del"><a href="javascript:void(0)"     ><img src="<?=SITE_TEMPLATE_PATH?>/images/basket_remove.png" /></a></div>
                    <? endif ?>
                    <?/* if ($arParams["SHOW_SUMMARY"] == "Y"): ?>
                                    <div class="bx_item_col_summ">
                                        <strong><?= $v["QUANTITY"] ?></strong> <?= $v["MEASURE_NAME"] ?> <?= GetMessage("TSB1_SUM") ?>
                                        <strong><?= $v["SUM"] ?></strong>
                                    </div>
                            <? endif*/ ?>
                        <? endif ?>
                        </div>
                <? endforeach ?>
            <? endforeach ?>
            </div>
            <div class="basket_res_line">
                <div class="left w45">
                    
                    <?$price_without_cur = explode(" ",$arResult['TOTAL_PRICE']);?> 
                    �����: <span class="result_price"><?= $price_without_cur[0] ?></span> ���.</div>
                <div class="right w45"><a href="/personal/cart/" class="basket_btn">������� � �������</a></div>
                <div class="clear"></div>
            </div>    

                    <? /*if ($arParams["PATH_TO_ORDER"] && $arResult["CATEGORIES"]["READY"]): ?>
                <div class="bx_button_container">
                    <a href="<?= $arParams["PATH_TO_ORDER"] ?>" class="bx_bt_button_type_2 bx_medium">
                <?= GetMessage("TSB1_2ORDER") ?>
                    </a>
                </div>
        <? endif */?>

        </div>

        <? if ($arParams["POSITION_FIXED"] == "Y"): ?>
            <script>sbbl.fixCartAfterAjax()</script>
        <? endif ?>
    <? endif ?>

    <?
    if ($arParams["POSITION_FIXED"] == "N")
        sbblSmallCartTemplate($arResult, $arParams);
    ?>

<?endif?>