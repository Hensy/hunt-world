<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("��������-������� \"��� �����\"");
?>
<div class="inner_wrapper_min">
                                <div class="items_block special_offers">
                                    <div class="delimiter">
                                        <span class="fOSC">����������� �����������</span>
                                        <span class="list_item_controls">
                                            <div class="dib w50 prev_btn"></div>
                                            <div class="dib w50 next_btn"></div>
                                        </span>
                                    </div>
                                    <?
                                    $APPLICATION->IncludeComponent(
                                            "lenal:arfilter.create", ".default", Array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => "2",
                                        "ARFILTER_NAME" => "specialsFilter",
                                        "PROPERTY_CODE" => "SPECIALOFFER",
                                        "PROPERTY_VALUE" => "��",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_GROUPS" => "Y"
                                            )
                                    );
                                    ?>
                                    <?
                                    $APPLICATION->IncludeComponent("bitrix:catalog.top", "slider_main_page", array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => "2",
                                        "ELEMENT_SORT_FIELD" => "sort",
                                        "ELEMENT_SORT_ORDER" => "asc",
                                        "ELEMENT_SORT_FIELD2" => "name",
                                        "ELEMENT_SORT_ORDER2" => "asc",
                                        "FILTER_NAME" => "specialsFilter",
                                        "HIDE_NOT_AVAILABLE" => "N",
                                        "ELEMENT_COUNT" => "10",
                                        "LINE_ELEMENT_COUNT" => "10",
                                        "PROPERTY_CODE" => array(
                                            0 => "MINIMUM_PRICE",
                                            1 => "MAXIMUM_PRICE",
                                            2 => "",
                                        ),
                                        "OFFERS_FIELD_CODE" => array(
                                            0 => "NAME",
                                            1 => "",
                                        ),
                                        "OFFERS_PROPERTY_CODE" => array(
                                            0 => "ARTNUMBER",
                                            1 => "COLOR_REF",
                                            2 => "SIZES_SHOES",
                                            3 => "SIZES_CLOTHES",
                                            4 => "MORE_PHOTO",
                                            5 => "",
                                        ),
                                        "OFFERS_SORT_FIELD" => "sort",
                                        "OFFERS_SORT_ORDER" => "asc",
                                        "OFFERS_SORT_FIELD2" => "id",
                                        "OFFERS_SORT_ORDER2" => "desc",
                                        "OFFERS_LIMIT" => "0",
                                        "VIEW_MODE" => "SECTION",
                                        "TEMPLATE_THEME" => "blue",
                                        "PRODUCT_DISPLAY_MODE" => "Y",
                                        "ADD_PICT_PROP" => "-",
                                        "LABEL_PROP" => "SPECIALOFFER",
                                        "OFFER_ADD_PICT_PROP" => "-",
                                        "OFFER_TREE_PROPS" => array(
                                            0 => "-",
                                        ),
                                        "SHOW_DISCOUNT_PERCENT" => "Y",
                                        "SHOW_OLD_PRICE" => "Y",
                                        "MESS_BTN_BUY" => "������",
                                        "MESS_BTN_ADD_TO_BASKET" => "� �������",
                                        "MESS_BTN_DETAIL" => "���������",
                                        "MESS_NOT_AVAILABLE" => "��� � �������",
                                        "SECTION_URL" => "",
                                        "DETAIL_URL" => "",
                                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "180",
                                        "CACHE_GROUPS" => "Y",
                                        "DISPLAY_COMPARE" => "Y",
                                        "CACHE_FILTER" => "N",
                                        "PRICE_CODE" => array(
                                            0 => "BASE",
                                        ),
                                        "USE_PRICE_COUNT" => "N",
                                        "SHOW_PRICE_COUNT" => "1",
                                        "PRICE_VAT_INCLUDE" => "Y",
                                        "CONVERT_CURRENCY" => "N",
                                        "BASKET_URL" => "/personal/cart/",
                                        "ACTION_VARIABLE" => "action",
                                        "PRODUCT_ID_VARIABLE" => "id_slider",
                                        "USE_PRODUCT_QUANTITY" => "N",
                                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                                        "PRODUCT_PROPS_VARIABLE" => "prop",
                                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                        "PRODUCT_PROPERTIES" => array(
                                        ),
                                        "OFFERS_CART_PROPERTIES" => array(
                                            0 => "ARTNUMBER",
                                            1 => "COLOR_REF",
                                            2 => "SIZES_SHOES",
                                            3 => "SIZES_CLOTHES",
                                        ),
                                        "PRODUCT_QUANTITY_VARIABLE" => "quantity"
                                            ), false
                                    );
                                    ?>
                                </div>
                                <div class="items_block new_items">
                                    <div class="delimiter">
                                        <span class="fOSC">�������</span>
                                        <span class="list_item_controls">
                                            <div class="dib w50 prev_btn"></div>
                                            <div class="dib w50 next_btn"></div>
                                        </span>
                                    </div>
                                    <?
                                    $APPLICATION->IncludeComponent(
                                            "lenal:arfilter.create", "", Array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => "2",
                                        "ARFILTER_NAME" => "news_items",
                                        "PROPERTY_CODE" => "NEWPRODUCT",
                                        "PROPERTY_VALUE" => "��",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_GROUPS" => "Y"
                                            )
                                    );
                                    ?>
                                    <?
                                    $APPLICATION->IncludeComponent("bitrix:catalog.top", "slider_main_page", array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => "2",
                                        "ELEMENT_SORT_FIELD" => "sort",
                                        "ELEMENT_SORT_ORDER" => "asc",
                                        "ELEMENT_SORT_FIELD2" => "name",
                                        "ELEMENT_SORT_ORDER2" => "asc",
                                        "FILTER_NAME" => "news_items",
                                        "HIDE_NOT_AVAILABLE" => "N",
                                        "ELEMENT_COUNT" => "12",
                                        "LINE_ELEMENT_COUNT" => "4",
                                        "PROPERTY_CODE" => array(
                                            0 => "MINIMUM_PRICE",
                                            1 => "MAXIMUM_PRICE",
                                            2 => "",
                                        ),
                                        "OFFERS_FIELD_CODE" => array(
                                            0 => "NAME",
                                            1 => "",
                                        ),
                                        "OFFERS_PROPERTY_CODE" => array(
                                            0 => "ARTNUMBER",
                                            1 => "COLOR_REF",
                                            2 => "SIZES_SHOES",
                                            3 => "SIZES_CLOTHES",
                                            4 => "MORE_PHOTO",
                                            5 => "",
                                        ),
                                        "OFFERS_SORT_FIELD" => "sort",
                                        "OFFERS_SORT_ORDER" => "asc",
                                        "OFFERS_SORT_FIELD2" => "id",
                                        "OFFERS_SORT_ORDER2" => "desc",
                                        "OFFERS_LIMIT" => "0",
                                        "VIEW_MODE" => "SECTION",
                                        "TEMPLATE_THEME" => "site",
                                        "PRODUCT_DISPLAY_MODE" => "Y",
                                        "ADD_PICT_PROP" => "MORE_PHOTO",
                                        "LABEL_PROP" => "NEWPRODUCT",
                                        "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                                        "OFFER_TREE_PROPS" => array(
                                            0 => "COLOR_REF",
                                            1 => "SIZES_SHOES",
                                            2 => "SIZES_CLOTHES",
                                        ),
                                        "SHOW_DISCOUNT_PERCENT" => "Y",
                                        "SHOW_OLD_PRICE" => "Y",
                                        "MESS_BTN_BUY" => "������",
                                        "MESS_BTN_ADD_TO_BASKET" => "� �������",
                                        "MESS_BTN_DETAIL" => "���������",
                                        "MESS_NOT_AVAILABLE" => "��� � �������",
                                        "SECTION_URL" => "",
                                        "DETAIL_URL" => "",
                                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "180",
                                        "CACHE_GROUPS" => "Y",
                                        "DISPLAY_COMPARE" => "N",
                                        "CACHE_FILTER" => "N",
                                        "PRICE_CODE" => array(
                                            0 => "BASE",
                                        ),
                                        "USE_PRICE_COUNT" => "N",
                                        "SHOW_PRICE_COUNT" => "1",
                                        "PRICE_VAT_INCLUDE" => "Y",
                                        "CONVERT_CURRENCY" => "N",
                                        "BASKET_URL" => "/personal/cart/",
                                        "ACTION_VARIABLE" => "action",
                                        "PRODUCT_ID_VARIABLE" => "id_section",
                                        "USE_PRODUCT_QUANTITY" => "Y",
                                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                                        "PRODUCT_PROPS_VARIABLE" => "prop",
                                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                        "PRODUCT_PROPERTIES" => array(
                                        ),
                                        "OFFERS_CART_PROPERTIES" => array(
                                            0 => "ARTNUMBER",
                                            1 => "COLOR_REF",
                                            2 => "SIZES_SHOES",
                                            3 => "SIZES_CLOTHES",
                                        )
                                            ), false
                                    );
                                    ?>


                                </div>
                            </div>
                            <div class="inner_wrapper">
                                <div class="fat_delimiter"></div>
                            </div>
                            <div class="inner_wrapper_min">
                                <div class="left w75">
                                    <div class="news_block">
                                        <div class="delimiter">
                                            <span class="fOSC">�������</span>
                                            <a href="/news/" class="news_link">��� �������</a>
                                        </div>
                                        <?
                                        $APPLICATION->IncludeComponent("bitrix:news.list", "main_page", array(
	"IBLOCK_TYPE" => "news",
	"IBLOCK_ID" => "1",
	"NEWS_COUNT" => "20",
	"SORT_BY1" => "ID",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "",
	"SORT_ORDER2" => "",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "j F Y",
	"SET_STATUS_404" => "N",
	"SET_TITLE" => "Y",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
	"ADD_SECTIONS_CHAIN" => "Y",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "Y",
	"PAGER_TEMPLATE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "�������",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
                                        ?>

                                    </div>
                                </div>
                                <div class="right w23">
                                    <div class="experts">
                                        <span>������� <br /> � ����������</span>
                                        <?
                                        $APPLICATION->IncludeComponent(
                                                "bitrix:news.list", "experts", Array(
                                            "IBLOCK_TYPE" => "experts",
                                            "IBLOCK_ID" => "7",
                                            "NEWS_COUNT" => "20",
                                            "SORT_BY1" => "",
                                            "SORT_ORDER1" => "",
                                            "SORT_BY2" => "",
                                            "SORT_ORDER2" => "",
                                            "FILTER_NAME" => "",
                                            "FIELD_CODE" => array(0 => "", 1 => "",),
                                            "PROPERTY_CODE" => array(0 => "SPECIALIZATION", 1 => "",),
                                            "CHECK_DATES" => "Y",
                                            "DETAIL_URL" => "",
                                            "AJAX_MODE" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_FILTER" => "N",
                                            "CACHE_GROUPS" => "Y",
                                            "PREVIEW_TRUNCATE_LEN" => "",
                                            "ACTIVE_DATE_FORMAT" => "",
                                            "SET_STATUS_404" => "N",
                                            "SET_TITLE" => "N",
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                            "PARENT_SECTION" => "",
                                            "PARENT_SECTION_CODE" => "",
                                            "INCLUDE_SUBSECTIONS" => "N",
                                            "PAGER_TEMPLATE" => "",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "N",
                                            "PAGER_TITLE" => "�������",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "N",
                                            "DISPLAY_DATE" => "N",
                                            "DISPLAY_NAME" => "Y",
                                            "DISPLAY_PICTURE" => "Y",
                                            "DISPLAY_PREVIEW_TEXT" => "Y",
                                            "AJAX_OPTION_ADDITIONAL" => ""
                                                )
                                        );
                                        ?>

                                        <p class="all_experts"><a href="#">��� ��������</a></p>
                                    </div>
                                    <div class="magazine_block">
                                        <a href="#"><img src="images/magazine.png"></a>
                                        <div class="magazine_block_description">
                                            <a href="#">������ ���� ������ �1 �14</a>
                                            <span>32,5 mb</span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="magazine_block_btn">
                                            <a class="left" href="#"><img src="images/magazine_pdf.png"></a>
                                            <a class="right" href="#"><img src="images/magazine_store.png"></a>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>



                                <div class="items_block last_ordered">
                                    <div class="delimiter">
                                        <span class="fOSC">��������� ���������� ������</span>
                                        <span class="list_item_controls">
                                            <div class="dib w50 prev_btn"></div>
                                            <div class="dib w50 next_btn"></div>
                                        </span>
                                    </div>
                                    <?
                                    $APPLICATION->IncludeComponent(
                                            "bitrix:catalog.top", "slider_main_page", Array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => "2",
                                        "ELEMENT_SORT_FIELD" => "PROPERTY_LAST_ORDER_ID",
                                        "ELEMENT_SORT_ORDER" => "desc",
                                        "ELEMENT_SORT_FIELD2" => "name",
                                        "ELEMENT_SORT_ORDER2" => "asc",
                                        "FILTER_NAME" => "",
                                        "HIDE_NOT_AVAILABLE" => "N",
                                        "ELEMENT_COUNT" => "4",
                                        "LINE_ELEMENT_COUNT" => "4",
                                        "PROPERTY_CODE" => array(0 => "MINIMUM_PRICE", 1 => "MAXIMUM_PRICE", 2 => "",),
                                        "OFFERS_FIELD_CODE" => array(0 => "NAME", 1 => "",),
                                        "OFFERS_PROPERTY_CODE" => array(0 => "ARTNUMBER", 1 => "COLOR_REF", 2 => "SIZES_SHOES", 3 => "SIZES_CLOTHES", 4 => "MORE_PHOTO", 5 => "",),
                                        "OFFERS_SORT_FIELD" => "sort",
                                        "OFFERS_SORT_ORDER" => "asc",
                                        "OFFERS_SORT_FIELD2" => "id",
                                        "OFFERS_SORT_ORDER2" => "desc",
                                        "OFFERS_LIMIT" => "0",
                                        "VIEW_MODE" => "SECTION",
                                        "TEMPLATE_THEME" => "blue",
                                        "PRODUCT_DISPLAY_MODE" => "Y",
                                        "ADD_PICT_PROP" => "MORE_PHOTO",
                                        "LABEL_PROP" => "SPECIALOFFER",
                                        "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                                        "OFFER_TREE_PROPS" => array(0 => "-",),
                                        "SHOW_DISCOUNT_PERCENT" => "Y",
                                        "SHOW_OLD_PRICE" => "Y",
                                        "MESS_BTN_BUY" => "������",
                                        "MESS_BTN_ADD_TO_BASKET" => "� �������",
                                        "MESS_BTN_DETAIL" => "���������",
                                        "MESS_NOT_AVAILABLE" => "��� � �������",
                                        "SECTION_URL" => "",
                                        "DETAIL_URL" => "",
                                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "180",
                                        "CACHE_GROUPS" => "Y",
                                        "DISPLAY_COMPARE" => "Y",
                                        "CACHE_FILTER" => "N",
                                        "PRICE_CODE" => array(0 => "BASE",),
                                        "USE_PRICE_COUNT" => "N",
                                        "SHOW_PRICE_COUNT" => "1",
                                        "PRICE_VAT_INCLUDE" => "Y",
                                        "CONVERT_CURRENCY" => "N",
                                        "BASKET_URL" => "/personal/cart/",
                                        "ACTION_VARIABLE" => "action",
                                        "PRODUCT_ID_VARIABLE" => "id_slider",
                                        "USE_PRODUCT_QUANTITY" => "Y",
                                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                                        "PRODUCT_PROPS_VARIABLE" => "prop",
                                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                        "PRODUCT_PROPERTIES" => array(),
                                        "OFFERS_CART_PROPERTIES" => array(0 => "ARTNUMBER", 1 => "COLOR_REF", 2 => "SIZES_SHOES", 3 => "SIZES_CLOTHES",)
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="inner_wrapper">
                                <div class="brands_slider">
                                    <?
                                    $APPLICATION->IncludeComponent(
                                            "bitrix:news.list", "brends_slider", Array(
                                        "IBLOCK_TYPE" => "brends",
                                        "IBLOCK_ID" => "8",
                                        "NEWS_COUNT" => "20",
                                        "SORT_BY1" => "",
                                        "SORT_ORDER1" => "",
                                        "SORT_BY2" => "",
                                        "SORT_ORDER2" => "",
                                        "FILTER_NAME" => "",
                                        "FIELD_CODE" => array(0 => "", 1 => "",),
                                        "PROPERTY_CODE" => array(0 => "", 1 => "",),
                                        "CHECK_DATES" => "Y",
                                        "DETAIL_URL" => "",
                                        "AJAX_MODE" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_FILTER" => "N",
                                        "CACHE_GROUPS" => "Y",
                                        "PREVIEW_TRUNCATE_LEN" => "",
                                        "ACTIVE_DATE_FORMAT" => "",
                                        "SET_STATUS_404" => "N",
                                        "SET_TITLE" => "Y",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                        "ADD_SECTIONS_CHAIN" => "Y",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "INCLUDE_SUBSECTIONS" => "Y",
                                        "DISPLAY_DATE" => "N",
                                        "DISPLAY_NAME" => "N",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "�������",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N"
                                            )
                                    );
                                    ?>

                                </div>
                            </div>
                            <div class="inner_wrapper_min last_order_city">

                                <?
                                $APPLICATION->IncludeComponent("bitrix:news.list", "last_order_cities", array(
                                    "IBLOCK_TYPE" => "city_list",
                                    "IBLOCK_ID" => "4",
                                    "NEWS_COUNT" => "5",
                                    "SORT_BY1" => "RAND",
                                    "SORT_ORDER1" => "RAND",
                                    "SORT_BY2" => "",
                                    "SORT_ORDER2" => "",
                                    "FILTER_NAME" => "",
                                    "FIELD_CODE" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "PROPERTY_CODE" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "ACTIVE_DATE_FORMAT" => "",
                                    "SET_STATUS_404" => "N",
                                    "SET_TITLE" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "�������",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => ""
                                        ), false
                                );
                                ?>

                            </div>
                            <div class="inner_wrapper_min pb20">
                                <div class="about left">
                                    <div class="big_title">� ��������</div>
                                    <div class="text">
                                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/seo_main.php"), false); ?>


                                    </div>
                                </div>
                                <div class="tabs left">
                                    
                                <span id="tab1" class="active">���������</span>
                <span id="tab2">facebook</span>
                <span id="tab3">twitter</span>
                <div class="tabs_content">

                    <div class="tab1">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?113"></script>

                        <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                            //VK.Widgets.Group("vk_groups", {mode: 0, width: "245", height: "250", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 20003922);
                        </script>
                    </div>
                    <div class="tab2">
                                <?// $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/facebook_plugin.php"), false); ?>

                        <!--<div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-width="245" data-height="250" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>-->
                    </div>
                    <div class="tab3">
                        twitter
                    </div>
                </div>    
                                    
                                    
                                </div>
                                <div class="clear"></div>
                            </div>    
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>